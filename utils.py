import pandas as pd
import numpy as np
import scipy.stats as stats
from scipy.optimize import minimize

########################################################################
# FUNCTIONS FOR READING IN FILES
########################################################################

def read_small_large_cap():
    file = 'data/Portfolios_Formed_on_ME_monthly_EW.csv'
    returns = pd.read_csv(file, header=0, index_col=0, na_values=-99.99)
    returns = returns[['Lo 10', 'Hi 10']].rename({"Lo 10": "Small Cap", "Hi 10": "Large Cap"}, axis=1)
    returns = returns/100
    n_months = len(returns)
    returns.index = pd.to_datetime(returns.index, format='%Y%m')
    returns.index = returns.index.to_period('M')
    return returns, n_months

# Average cap size of firms in each industry
def read_avg_cap_size():
    avg_cap_size_file = 'data/ind30_m_size.csv'
    ind_avg_cap_size = pd.read_csv(avg_cap_size_file, index_col=0, header=0)
    ind_avg_cap_size.index = pd.to_datetime(ind_avg_cap_size.index, format='%Y%m')
    ind_avg_cap_size.index = ind_avg_cap_size.index.to_period('M')
    ind_avg_cap_size.columns = [col.strip(" ") for col in ind_avg_cap_size.columns]
    return ind_avg_cap_size

def read_hedge_fund_indices():
    file = 'data/edhec-hedgefundindices.csv'
    returns = pd.read_csv(file, index_col=0, header=0, parse_dates=True)
    returns = returns/100
    returns.index = returns.index.to_period('M')
    return returns

# Number of firms in each industry
def read_num_firms():
    num_firms_file = 'data/ind30_m_nfirms.csv'
    ind_num_firms = pd.read_csv(num_firms_file, index_col=0, header=0)
    ind_num_firms.index = pd.to_datetime(ind_num_firms.index, format='%Y%m')
    ind_num_firms.index = ind_num_firms.index.to_period('M')
    ind_num_firms.columns = [col.strip(" ") for col in ind_num_firms.columns]
    return ind_num_firms

def read_ind30():
    file = 'data/ind30_m_vw_rets.csv'
    returns = pd.read_csv(file, header=0, index_col=0, parse_dates=True)
    returns.index = pd.to_datetime(returns.index, format='%Y%m')
    returns.index = returns.index.to_period('M')
    returns = returns/100
    col_names = [col.split(" ")[0] for col in returns.columns]
    returns.columns = col_names
    return returns

########################################################################
# FUNCTIONS FOR COMPUTING STATISTICS
########################################################################

def compute_moment(returns, moment):
    r_demean = ((returns - returns.mean())**moment).mean()
    sigma = returns.std(ddof=0)
    r_moment = r_demean/(sigma**moment)
    return r_moment

def compute_wealth_index(returns, plot=False):
    wealth_index = 1*(1 + returns).cumprod()
    prev_max = wealth_index.cummax()
    if plot:
        wealth_index.plot.line()
        prev_max.plot.line()
    return wealth_index, prev_max

def compute_drawdown(returns, plot=False):
    wealth_index, prev_max = compute_wealth_index(returns)
    drawdown = (wealth_index - prev_max)/prev_max
    if plot:
        drawdown.plot.line()
    return drawdown
        
def is_normal(data, alpha=0.01):
    stat, p_val = stats.jarque_bera(data)
    return p_val > alpha

def annual_return(returns, periods_per_year):
    n_periods = len(returns)
    compounded_growth = (1 + returns).prod()
    period_return = compounded_growth**(1/n_periods)
    annual_return = (period_return**periods_per_year) - 1
    return annual_return

def annual_vol(returns, periods_per_year):
    period_vol = returns.std()
    annual_vol = period_vol*np.sqrt(periods_per_year)
    return annual_vol

def sharpe_ratio(returns, ann_risk_free_rate, periods_per_year):
    period_risk_free_rate = (1 + ann_risk_free_rate)**(1/periods_per_year) - 1
    period_excess_return = returns - period_risk_free_rate
    ann_excess_return = annual_return(period_excess_return, periods_per_year)
    ann_vol = annual_vol(returns, periods_per_year)
    sharpe_ratio = ann_excess_return/ann_vol
    return sharpe_ratio

########################################################################
# FUNCTIONS FOR PORTFOLIO RETURN & VOLATILITY
########################################################################

def total_mkt_return(ind_returns, ind_avg_cap_size, ind_num_firms):
    ind_cap_size = ind_avg_cap_size * ind_num_firms
    total_mkt_cap = ind_cap_size.sum(axis=1)
    ind_cap_weight = ind_cap_size.divide(total_mkt_cap, axis=0)
    tot_mkt_return = (ind_cap_weight * ind_returns).sum(axis=1)
    return tot_mkt_return

def portfolio_return(weights, asset_returns):
    port_return = weights.T @ asset_returns
    return port_return

def portfolio_vol(weights, cov):
    port_vol = np.sqrt(weights.T @ cov @ weights)
    return port_vol

########################################################################
# FUNCTIONS FOR VOLATILITY STATISTICS
########################################################################

def semi_dev(returns):
    return returns[returns<0].std(ddof=0)

def historic_var(returns, alpha=5):
    """
    Bottom alpha percentile of returns.
    """
    if isinstance(returns, pd.DataFrame):
        return returns.aggregate(historic_var, alpha=alpha)
        
    elif isinstance(returns, pd.Series):
        # Generally speaking, the VaR refers to 'losses' so we multiply by negative 1 
        return -np.percentile(returns, alpha)
    
    else:
        raise TypeError("Expected Dataframe or Series")
        
def gaussian_var(returns, alpha=5, modified=False):
    z = stats.norm.ppf(alpha/100)
    if modified:
        skew = compute_moment(returns, 3)
        kurt = compute_moment(returns, 4)
        z = (z + 
                (z**2 - 1)*skew/6 +
                (z**3 - 3*z)*(kurt-3)/24 - 
                (2*z**3 - 5*z)*(skew**2)/36
            )
    return -(z*returns.std(ddof=0) + returns.mean())

def cvar(returns, alpha=5):
    if isinstance(returns, pd.Series):
        lower_bound = -historic_var(returns, alpha)
        return -returns[returns < lower_bound].mean()
    elif isinstance(returns, pd.DataFrame):
        return returns.aggregate(cvar, alpha=alpha)
    else:
        raise TypeError("Expected Dataframe or Series")
        
######################################################################################
# FUNCTIONS FOR FINDING PORTFOLIOS ON THE EFFICIENT FRONTIER & CAPITAL MARKET LINE
######################################################################################

# msr function finds portfolio weights with the maximum sharpe ratio
def msr(asset_returns, cov, rf_rate):
    
    def negative_sharpe_ratio(weights, asset_returns, cov, rf_rate):
        port_return = portfolio_return(weights, asset_returns)
        excess_port_return = port_return - rf_rate
        port_vol = portfolio_vol(weights, cov)
        neg_sharpe = -(excess_port_return/port_vol)
        return neg_sharpe
    
    n = len(asset_returns)
    init_guess = np.repeat(1/n, n)
    bounds = ((0, 1),)*n

    weight_sum_is_one = {'type': 'eq',
                         'fun': lambda weights: np.sum(weights) - 1
                        }
    result = minimize(negative_sharpe_ratio, 
                       init_guess,
                       args=(asset_returns, cov, rf_rate,),
                       method="SLSQP",
                       options={'disp': False},
                       constraints=(weight_sum_is_one),
                       bounds=bounds
                       )
    weights = result.x
    return weights


def optimal_weights(n_points, asset_returns, cov):
    target_returns = np.linspace(asset_returns.min(), asset_returns.max(), n_points)
    weights = [minimize_vol(target_return, asset_returns, cov) for target_return in target_returns]
    return weights

"""
function finds the weights that generate the target return, while keeping volatility minmial
"""    
def minimize_vol(target_return, asset_returns, cov):
    
    n = len(asset_returns)
    init_guess = np.repeat(1/n, n)
    bounds = ((0, 1),)*n
    return_is_target = {'type': 'eq',
                        'args': (asset_returns,),
                        'fun':lambda weights, asset_returns: target_return - portfolio_return(weights, asset_returns)    
                        }
    weight_sum_is_one = {'type': 'eq',
                         'fun': lambda weights: np.sum(weights) - 1
                        }
    result = minimize(portfolio_vol, 
                       init_guess,
                       args=(cov,),
                       method="SLSQP",
                       options={'disp': False},
                       constraints=(return_is_target, weight_sum_is_one),
                       bounds=bounds
                       )
    weights = result.x
    return weights


def plot_ef(n_points, asset_returns, cov, rf_rate=0.1, plot_ew=False, plot_gmv=False, plot_msr=False):
    
    num_assets = len(asset_returns)
    weights = optimal_weights(n_points, asset_returns, cov)
    port_returns = [portfolio_return(weight, asset_returns) for weight in weights]
    port_vols = [portfolio_vol(weight, cov) for weight in weights]
    ef = pd.DataFrame({"Returns": port_returns, "Volatility": port_vols})
    ax = ef.plot.line(x="Volatility", y="Returns", style='.-', )
    ax.set_xlim(0)
    
    # global minimum variance portfolio - computes weights for minimum variance portfolio
    if plot_gmv:
        weights = msr(np.repeat(1, num_assets), cov, 0)
        gmv_port_return = portfolio_return(weights, asset_returns)
        gmv_port_vol = portfolio_vol(weights, cov)
        ax.scatter(gmv_port_vol, gmv_port_return, color='blue', s=75)
        ax.text(gmv_port_vol, gmv_port_return, 'gmv', size=16)
        print("global minimum variance portfolio weights " + str(np.round(weights, 4)) + '\n')
    
    # equal weight portfolio
    if plot_ew:
        weights = np.repeat(1/num_assets, num_assets)
        ew_port_return = portfolio_return(weights, asset_returns)
        ew_port_vol = portfolio_vol(weights, cov)
        ax.scatter(ew_port_vol, ew_port_return, color='green', s=75)
        ax.text(ew_port_vol, ew_port_return, 'ew', size=16)
        print("equal weight portfolio weights " + str(np.round(weights, 4)) + '\n')

    
    # maximum sharpe ratio portfolio - computes weights for tangency portfolio
    if plot_msr:
        weights = msr(asset_returns, cov, rf_rate)
        msr_port_return = portfolio_return(weights, asset_returns)
        msr_port_vol = portfolio_vol(weights, cov)
        x_values = [msr_port_vol, 0]
        y_values = [msr_port_return, rf_rate]
        ax.plot(x_values, y_values, 'bo', linestyle="--", color="red")
        ax.text(msr_port_vol, msr_port_return, 'msr', size=16)
        print("maximum sharpe ratrio portfolio weights " + str(np.round(weights, 4)) + '\n')




